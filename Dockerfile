FROM debian:unstable-slim

ENV TZ=Europe/Amsterdam
ENV LANG=en_GB.UTF-8

RUN apt-get update && apt-get install -y ca-certificates && apt-get clean all
ADD sources.list /etc/apt/sources.list
RUN  apt-get update && \
  apt-get install -y firefox curl bzip2 libxt6 pulseaudio-utils mesa-utils locales fonts-dejavu fonts-noto libasound2 wget && \
  apt-get clean all

ADD locale.gen /etc/locale.gen
RUN /usr/sbin/locale-gen

ADD pulseaudio.client.conf /etc/pulse/client.conf
WORKDIR /
RUN wget -q -O  - "https://download.mozilla.org/?product=firefox-nightly-latest-ssl&os=linux64&lang=en-US" | tar -xj -C / -f -

RUN mkdir /firefox/distribution
ADD policies.json /firefox/distribution/policies.json

RUN useradd -u 2001 -m -s /bin/bash c-fox
USER c-fox
CMD /firefox/firefox
