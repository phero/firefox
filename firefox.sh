#!/bin/bash

DOWNLOADS_DIR=~/Downloads/firefox
DOCKER_IMAGE=firefox:nightly
PROFILE=default-nightly
MOZILLA_CONF_DIR=~/.config/firefox/$PROFILE
MOZILLA_CACHE_DIR=~/.config/firefox/$PROFILE-cache
EXTRA_OPTS="--no-remote"
USER=c-fox
PULSEAUDIO_SOCKET=/tmp/pulseaudio.socket

/usr/bin/mkdir -m 700 -p $MOZILLA_CONF_DIR $MOZILLA_CACHE_DIR $DOWNLOADS_DIR
/usr/bin/setfacl -m u:$USER:rwX $MOZILLA_CONF_DIR $MOZILLA_CACHE_DIR $DOWNLOADS_DIR
/usr/bin/xhost +SI:localuser:$USER
/usr/bin/pactl load-module module-native-protocol-unix socket=$PULSEAUDIO_SOCKET auth-anonymous=1

/usr/bin/podman run --rm -d --name firefox -h firefox \
  --read-only \
  --security-opt=no-new-privileges \
  -e DISPLAY="$DISPLAY" \
  -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
  -e PULSE_SERVER=unix:$PULSEAUDIO_SOCKET \
  -e PULSE_COOKIE=/tmp/pulseaudio.cookie \
  -v $PULSEAUDIO_SOCKET:$PULSEAUDIO_SOCKET \
  -v $MOZILLA_CONF_DIR:/home/$USER/.mozilla \
  --mount type=tmpfs,destination=/home/$USER/.cache \
  --mount type=tmpfs,destination=/tmp \
  -v $DOWNLOADS_DIR:/home/$USER/Downloads \
  --device /dev/dri:rw \
  --group-add=nogroup \
  -e TZ=Europe/Amsterdam \
  -e LANG=en_GB.UTF-8 \
  -v ~/.fonts.conf:/home/user/.fonts.conf:ro \
  --net slirp4netns:enable_ipv6=true \
  $DOCKER_IMAGE /firefox/firefox $EXTRA_OPTS "$@"
