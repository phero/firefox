# containerized firefox

Containerized hardened firefox nightly. Script made for `podman`.

Restrictions are configured in `policies.json`.

Comments and contributions are welcome :)
